import { Comparator } from './comparator.service'

describe('Comparator service', () => {
  const [
    a50,
    a30,
    a50s,
    a50s5g,
    a515g,
    a51s5g,
    masloBriliant,
    masloBriliant2,
    masloBiser,
    masloBiser2,
    asusSetec,
    asusNeptun,
  ] = [
    'Samsung A50',
    'Samsung A30',
    'Samsung A50s',
    'Samsung A50s 5G',
    'Samsung A51 5G',
    'Samsung A51s 5G',
    'Maslo Brilijant',
    'Maslo Briliant 1l',
    'Maslo Biser',
    'Maslo Biser 1l',
    'ASUS ROG Zephyrus 16 GX650RX-LO154X / Win 11 Pro',
    'ASUS ROG Zephyrus Duo 16 GX650RX-LO154X 16" R9-6900HX/32GB/2TB/RTX 3080Ti 16GB',
  ]
  const shouldBeTheSame = 'Should be in the same group'
  const shouldNotBeTheSame = 'Should not be in the same group'

  const comparator = new Comparator()

  it(shouldBeTheSame, () => {
    expect(comparator.belongToSameGroup(a50, a50s)).toBe(true)
  })

  it(shouldBeTheSame, () => {
    expect(comparator.belongToSameGroup(masloBriliant, masloBriliant2)).toBe(
      true,
    )
  })

  it(shouldBeTheSame, () => {
    expect(comparator.belongToSameGroup(masloBiser, masloBiser2)).toBe(true)
  })

  it(shouldBeTheSame, () => {
    expect(comparator.belongToSameGroup(a50, a50s5g)).toBe(true)
  })

  it(shouldBeTheSame, () => {
    expect(comparator.belongToSameGroup(asusSetec, asusNeptun)).toBe(true)
  })

  it(shouldBeTheSame, () => {
    expect(comparator.belongToSameGroup(a515g, a51s5g)).toBe(true)
  })

  it(shouldNotBeTheSame, () => {
    expect(comparator.belongToSameGroup(a515g, a50s5g)).toBe(false)
  })

  it(shouldNotBeTheSame, () => {
    expect(comparator.belongToSameGroup(a50s5g, a515g)).toBe(false)
  })

  it(shouldNotBeTheSame, () => {
    expect(comparator.belongToSameGroup(a50, a30)).toBe(false)
  })

  it(shouldNotBeTheSame, () => {
    expect(comparator.belongToSameGroup(a50, a515g)).toBe(false)
  })

  it(shouldNotBeTheSame, () => {
    expect(comparator.belongToSameGroup(masloBriliant, masloBiser)).toBe(false)
  })
})
