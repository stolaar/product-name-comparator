import { compareTwoStrings as ssCompareTwoStrings } from 'string-similarity'
import { removeEqualStrings } from '../utils/remove-equal-strings'

export class Comparator {
  constructor(private confidence = 0.55) {}

  compareTwoStrings(strOne: string, strTwo: string): number {
    if (!strOne || !strTwo) return 0
    if (strOne === strTwo) return 1
    const {
      totalWords: [strOneLength, strTwoLength],
      totalEqualWords: [strOneEquals, strTwoEquals],
      totalDiffWords: [strOneTotalDiffs, strTwoTotalDiffs],
      diffs: [strOneDiff, strTwoDiff],
    } = removeEqualStrings(strOne, strTwo)

    const sumOfEquals = strOneEquals + strTwoEquals
    const sumOfDiffs = strOneTotalDiffs + strTwoTotalDiffs

    const equalsMoreThanHalf =
      strOneEquals > strOneLength / 2 || strTwoEquals > strTwoLength / 2

    if (
      !strOneDiff ||
      !strTwoDiff ||
      (sumOfDiffs < sumOfEquals && equalsMoreThanHalf && sumOfDiffs > 2)
    )
      return 1
    return ssCompareTwoStrings(strOneDiff, strTwoDiff)
  }

  belongToSameGroup(strOne: string, strTwo: string): boolean {
    const confidence = this.compareTwoStrings(strOne, strTwo)
    console.log(strOne, strTwo, confidence)
    return confidence >= this.confidence
  }
}
