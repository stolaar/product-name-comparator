export interface IStringStats {
  totalEqualWords: [number, number]
  totalWords: [number, number]
  totalDiffWords: [number, number]
  diffs: [string, string]
}

export const removeEqualStrings = (
  strOne: string,
  strTwo: string,
  delimiter = ' ',
): IStringStats => {
  const [strArrayOne, strArrayTwo] = [
    strOne.split(delimiter),
    strTwo.split(delimiter),
  ]

  const [strOneDiffs, strTwoDiffs] = [
    strArrayOne.filter((word) => !strArrayTwo.includes(word)),
    strArrayTwo.filter((word) => !strArrayOne.includes(word)),
  ]

  return {
    totalWords: [strArrayOne.length, strTwoDiffs.length],
    totalEqualWords: [
      strArrayOne.length - strOneDiffs.length,
      strArrayTwo.length - strTwoDiffs.length,
    ],
    totalDiffWords: [strOneDiffs.length, strTwoDiffs.length],
    diffs: [strOneDiffs.join(delimiter), strTwoDiffs.join(delimiter)],
  }
}
